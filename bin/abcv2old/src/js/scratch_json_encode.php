<?php
$images = array(
"001" => array(
						"id" 				=> 	"001",
						"name" 			=>	"Jim Beam's Choice",
						"size" 			=>	"1.75L",
						"regprice" 	=> 	"33.95",
						"savings" 	=> 	"4.00",
						"saleprice" => 	"29.95",
						"url_image" => 	"png/jim_beam_choice.png"
						),
"002" => array(
						"id"				=> 	"002",
						"name"			=> 	"Jim Beam Kentucky Fire",
						"size"			=> 	".75L",
						"regprice"	=> 	"17.95",
						"savings"		=> 	"3.00",
						"saleprice"	=> 	"14.95",
						"url_image"	=> 	"png/JimBeamKYFire.png"
						),
"003" => array(
						"id"				=>	"003",
						"name"			=>	"Elijah Craig",
						"size"			=>	"1.75L",
						"regprice"	=> 	"49.95",
						"savings"		=>	"5.00",
						"saleprice"	=> 	"44.95",
						"url_image"	=> 	"png/Elijah_Craig.png"
						),
"004" => array(
						"id"				=>		"004",
						"name"			=> 	"Wild Turkey 101",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/WildTurkey101.png"
						),
"005" => array(
						"id"				=>		"005",
						"name"			=> 	"Skinny Girl Sweet Ariata",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/SkinnygirlSweetAriata.png"
						),
"006" => array(
						"id"				=>		"006",
						"name"			=> 	"Cazadores Reposado",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/CazadoresReposado.png"
						),
"007" => array(
						"id"				=>		"007",
						"name"			=> 	"Tuaca",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Tuaca.png"
						),
"008" => array(
						"id"				=>		"008",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
"009" => array(
						"id"				=>		"009",
						"name"			=> 	"Southern Comfort 100",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/southern-comfort-100.png"
						),
"010" => array(
						"id"				=>		"010",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
);
"011" => array(
						"id"				=>		"010",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
);
"012" => array(
						"id"				=>		"010",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
);
"013" => array(
						"id"				=>		"010",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	    => 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	    => 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
);
"014" => array(
						"id"				=>		"010",
						"name"			=> 	"Evan Williams Honey Reserve",
						"size"			=> 	"1.75L",
						"regprice"	=> 	"46.95",
						"savings"		=> 	"4.00",
						"saleprice"	=> 	"42.95",
						"url_image"	=> 	"png/Evan Williams Honey Reserve.png"
						),
);
echo json_encode($images);
?>