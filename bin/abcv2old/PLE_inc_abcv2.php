<?php
$v2template = <<<'HTML'
<html><head><meta charset="utf-8">
    <style>html, body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;margin:0; height:100%; width:100%;padding:0;}</style>
    <script type="text/javascript">
            window.contentType = 'slideshow';
        seance = {
            'config':function(){var x=this;
                document.getElementById("title-area").textContent = "August Specials";
                x.RES   = {w:1280,h:768};
                x.jdata = REPLSTR__JDATA;
                x.jlen = x.jdata.length;
                x.PRES_START  = REPLSTR__PRESSTART;
                x.PRES_END    = REPLSTR__PRESEND;
                x.TIMER_DELAY = 1000;
                x.TIMER_SHOW  = 9000;
                x.GROW_SPEED  = 2000; //fadespeed + timer_show = total loop time
                x.FADE_SPEED  = 2000;
                x.GOPA_SPEED  = 2000;
                x.FOPA_SPEED  = 2000;
                x.URL_SLUG    = "http://vi-digital.com/lib/abc/ABC-Cherry/";
                x.LOC_SLUG    = "content/";
                x.NOIMG_LOC  = "!no_image_ABC.svg";
                x.NOIMG_RMT  = "http://vi-digital.com/lib/abc/ABC-Cherry/!no_image_ABC.svg";
                x.ID_dynstyle = document.getElementById('dynstyle');
                x.ID_gridcontainer = document.getElementById("grid-container");
                x.ID_presentation = document.getElementById("presentation");
                x.gridboxes= [];
                x.Pi=0;
            }
            ,'init':function(){
                var x=this;
                x.config();
                x.buildPwraps();
                x.setDynStyle();
                x.buildPwraps();
                x.p_wraps = document.getElementsByClassName('Pwrap');
                x.slide_loop();
            }
            ,'buildPwraps':function(){
                var x=this,gridhtml = '', jd= x.jdata;
                for (var j = 0; j < x.jlen; j++) {
                    gridhtml += '<div class="cell">';
                    gridhtml += '<img src="';
                    gridhtml += x.LOC_SLUG + jd[j].filename;
                    gridhtml += '" onerror="seance.imgError1(this);"';
                    gridhtml += ' class="item-img"></div>';
                    x.ID_gridcontainer.innerHTML = gridhtml;
                    var preshtml = "";
                    for (var i = (x.PRES_START - 1); i <= (x.PRES_END - 1); i++) { //loop length = array length
                        preshtml += '<div id="Pwrapper' + (i + 1) + '" class="Pwrap shrunk">'
                                + '<div class="Pleft">'
                                + '<img src="' + x.LOC_SLUG + jd[i].filename + '" onerror="seance.imgError1(this);">'
                                + '</div>'
                                + '<div class="Pright">'
                                + '<div class="Ptitle">' + jd[i].name + '</div><hr>'
                                + '<div class="Pregprice">Regular Price: $' + jd[i].prices.retail + '</div>'
                                + '<div class="Psavings">Savings: $' + jd[i].prices.savings + '</div>'
                                + '<div class="Psaleprice">Sale Price: $' + jd[i].prices.discount + '</div>'
                                + '</div></div>';
                    }
                    x.ID_presentation.innerHTML = preshtml;
                }
            }
            ,'setDynStyle':function(){
                var x = this
                        ,G = x.GROW_SPEED
                        ,F = x.FADE_SPEED
                        ,GO = x.GOPA_SPEED
                        ,FO = x.FOPA_SPEED
                        ,styles = ""
                        ,n = x.jdata.length
                        ,s_maxheight = 240
                        ,s_flexbasis = 15
                        ;
                console.log(n);
                if ( n >= 1 && n <= 3 ){ s_maxheight = 540; s_flexbasis = 19;}
                else if (n >= 4   && n <= 6 ){ s_maxheight = 420; s_flexbasis = 28;}
                else if (n >= 7   && n <= 10 ){ s_maxheight = 340; s_flexbasis = 17;}
                else if (n >= 11  && n <= 21 ){ s_maxheight = 190; s_flexbasis = 16;}
                else if (n >= 22  && n <= 28 ){ s_maxheight = 150; s_flexbasis = 13;}
                else if (n >= 29  && n <= 31 ){ s_maxheight = 110; s_flexbasis = 14;}
                else if (n >= 32  && n <= 60 ){ s_maxheight = 140; s_flexbasis = 11;}
                else if (n >= 61  && n <= 120){ s_maxheight = 100; s_flexbasis = 6;}
                else if (n >= 121 && n <= 180){ s_maxheight = 60;  s_flexbasis = 4;}
                styles += '.cell {flex-basis:'+s_flexbasis+'%;}\r\n';
                styles +='.item-img {height: '+s_maxheight+'px;width: auto;}\r\n';
                styles +='.Pwrap{\r\n';
                styles +='    -webkit-transform:scale(0,0);transform:scale(0,0);opacity: 0;\r\n';
                styles +='    transition:-webkit-transform '+G+'ms, opacity '+GO+'ms 300ms;\r\n';
                styles +='    transition:        transform '+G+'ms, opacity '+GO+'ms 300ms;\r\n';
                styles +='}\r\n';
                styles +='.grown{\r\n';
                styles +='    -webkit-transform:scale(1,1);transform:scale(1,1);opacity: 1;\r\n';
                styles +='    transition:-webkit-transform '+G+'ms, opacity '+GO+'ms 300ms;\r\n';
                styles +='    transition:        transform '+G+'ms, opacity '+GO+'ms 300ms;\r\n';
                styles +='}\r\n';
                styles +='.shrunk{\r\n';
                styles +='    -webkit-transform:scale(0,0);transform:scale(0,0);opacity: 0;\r\n';
                styles +='    transition:-webkit-transform '+F+'ms, opacity '+FO+'ms 300ms;\r\n';
                styles +='    transition:        transform '+F+'ms, opacity '+FO+'ms 300ms;\r\n';
                styles +='}\r\n';
                styles +='#presentation, body{\r\n';
                styles +='    width:'+x.RES.w+'px;\r\n';
                styles +='    height:'+x.RES.h+'px;\r\n';
                styles +='}\r\n';
                x.ID_dynstyle.innerHTML = styles;
            }
            ,'applyStyles' : function(n){
                n.style.backgroundColor = "#ECECEC";
                n.style.height = "24px";
                n.style.fontSize = "12px";
                n.style.fontWeight = "lighter";
            }
            ,'summon':function(element, start, STOP, duration) {
                var invocation,focus=Math.round(duration/100),psiLevel=0;
                if (start < STOP) {console.log('showing');
                    for (var u = start; u <= STOP; u++) {
                        (function () {var orb = u;
                            setTimeout(function () {Ghostlier(element, orb, 1)
                            }, psiLevel * focus);}());psiLevel++;}
                } else {console.log('hiding');
                    for (var v = start; v >= STOP; v--) {
                        (function () {var orb = v;
                            setTimeout(function () {Ghostlier(element, orb, 0)
                            }, psiLevel * focus);}());psiLevel++;}
                }
                function Ghostlier(ghost, ghostliness, realm) {
                    invocation = 'opacity:' + ghostliness / 100
                            + ';filter:alpha(opacity=' + ghostliness + ');';
                    $(ghost).attr("style", invocation);
                    if(realm==0 && ghost.style.opacity<=.02){ghost.style.visibility = "hidden";}
                    else{ghost.style.visibility = "visible";}
                }
            }
            ,'slide_loop':function(){
                var x=this, thisP = x.p_wraps[x.Pi];
                if (x.Pi < x.PRES_END){x.Pi++;}else{x.Pi=0;}
                if (x.Pi <= x.PRES_END) {
                    setTimeout(function(){x.fadein(thisP);}, x.TIMER_DELAY);
                    setTimeout(function(){x.fadeout(thisP);}, x.TIMER_SHOW);
                    setTimeout(function(){x.slide_loop();}, x.TIMER_SHOW+x.FADE_SPEED);
                }
            }
            ,'fadein':function(E){
                E.classList.remove('shrunk');
                E.classList.add('grown');
            }
            ,'fadeout':function(E){
                E.classList.remove('grown');
                E.classList.add('shrunk');
            }


            ,'imgError1': function(image){console.log("==imgError1 : TRYING REMOTE SRC ==", image);
                var fname = image.src.match(/.*\/(.*\.png|gif|svg)/);
                image.src = null;image.onerror = null;image.removeAttribute("onerror");
                image.setAttribute("onerror", "seance.imgError2(this);");
                image.src = this.URL_SLUG+fname[1];return true;
            }
            ,'imgError2': function(image) {console.log("==imgError2 : TRYING SVG LOCAL ==");
                image.src= null;image.onerror = null;image.removeAttribute("onerror");
                image.setAttribute("onerror", "seance.imgError3(this);");
                image.src = this.NO_IMG_LOC;return true;
            }
            ,'imgError2': function(image) {console.log("==imgError3 : TRYING SVG REMOTE ==");
                image.src= null;image.onerror = null;image.removeAttribute("onerror");
                image.src = this.NO_IMG_RMT;return true;
            }
        };
        function startSlideshow(){
            seance.init();
        }

    </script>
</head><body>
<style type="text/css">
    body { background:#fff;box-sizing: border-box; }
    #presentation{ position:absolute;top:0;bottom:0;left:0;right:0; }
    #title-area{width: 100%;height: 90px;padding-top: 16px;
        background: #f00; color: #FFF;
        font-weight: bolder;
        font-size: 60px;
        font-family: Arial, sans-serif;
        text-align:center;
        text-shadow:
        4px  4px 0 #c40000,
        -1px -1px 0 #c40000,
        1px -1px 0 #c40000,
        -1px  1px 0 #c40000,
        1px  1px 0 #c40000;
    }
    #grid-container {
        background:#3e3e3e;
        position: relative;
        height: 662px;
        display:flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }
    .cell {
        position:relative;width:auto;height:auto;margin:2px;
        align-items: center;
        overflow:visible;
        flex-basis:6%;
    }
    .item-img {
        position: absolute;top:0;bottom:0;left:0;right:0;
        margin: 0 auto;
        max-width:100%;max-height:100%;
    }
    .Pwrap {
        /*display: none;*/
        position: absolute;top: 55%; left: 50%;
        margin: -250px 0 0 -400px;
        height: 500px;width:  800px;
        background: rgba(246, 246, 246, 0.95);
        border: 3px solid #aaaaaa; border-radius: 16px;
        -webkit-box-shadow: -6px 6px 15px 0 rgba(0,0,0,0.4);
        box-shadow:         -6px 6px 15px 0 rgba(0,0,0,0.4);
        /* -webkit-transform:scale(0,0); transform:scale(0,0);*/
        opacity:0.1;
    }
    .Pright{float:left; height: 480px; width: 400px; padding: 10px;}
    .Pleft {float:left; height: 460px; width: 300px; padding: 20px; max-height: 100%;}
    .Pleft img{display: block;max-height: 100%;margin: 0 auto;}
    .Ptitle,.Pregprice,.Psaleprice,.Psavings{color: #666;font-family: 'Arial', sans-serif;font-weight: bolder;}
    .Ptitle{font-size:43px; line-height:59px;color: #000;}
    .Pregprice{font-size:32px; line-height:79px;}
    .Psavings{font-size:32px; line-height:79px;  }
    .Psaleprice{font-size:38px; line-height:82px;color: red; font-weight: bold;}
</style>
<style id="dynstyle"></style>
<div id="title-area"></div>
<div id="grid-container"></div>
<div id="presentation"></div>

<!--  <script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>  -->
<!--<script>window.jQuery || document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"><\/script>');</script>-->
<script type="text/javascript">


</script></body></html>
HTML;

