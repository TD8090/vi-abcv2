<div id="vi-fix" style="position: relative; top: 0px; left: 0px;">
    <style type="text/css">@font-face {font-family: "Avenir";src:  url(Avenir-Book.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Book.ttf) format("truetype");}
        @font-face {font-family: "Avenir Light";src:  url(Avenir-Light.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Light.ttf) format("truetype");}
        @font-face {font-family: "Avenir Medium";src:  url(Avenir-Medium.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Medium.ttf) format("truetype");}
        @font-face {font-family: "Avenir Heavy";src:  url(Avenir-Heavy.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Heavy.ttf) format("truetype");}
        @font-face {font-family: "Avenir Black";src:  url(Avenir-Black.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Black.ttf) format("truetype");}
        body {
            background-color:#FFFFFF;
            border:none;
            color:#4a3728;
            margin:0;
            font-family: "Avenir", sans-serif;
            -webkit-font-smoothing: antialiased;
        }
        table, td {
            font-family: "Avenir Medium", sans-serif;
            font-size: 14px; vertical-align:top;
            text-align: left;
        }
        table {border-collapse:collapse;}
        .middle {background-color:#4a3728; width:5px;}
        .left-side img, .right-side img {width:100%; height:auto;}
        .left-side, .right-side {text-align:center; width:49.7%;}

        .header {font-size:28px; text-transform:uppercase;}
        .sm-header {font-size:21px; text-transform:capitalize;}
        .header, .sm-header {color:#febc11; font-weight:bold; font-family: "Avenir Heavy";}
        .header-sup {text-transform:none !important; font-size:17px; font-weight:bold; color:#775e4b; font-family: "Avenir Light";}
        .item-up, .item, .price {color:#4a3728; font-size:18px;  font-family: "Avenir Black"; letter-spacing:1px;	-webkit-font-smoothing: antialiased;}

        .item-up {text-transform:uppercase;}
        .item { text-transform:capitalize;}
        .price { text-align:right;}

        .desc {color:#775e4b; font-size:15px; font-family: "Avenir Light"; font-weight:bold;}
        .cal {color:#c3a385; font-size:18px; text-transform:Capitalize; font-family: "Avenir Light";}

        table table.main {width:98%; height:510px; margin:0 auto; text-align:left;}
        #table1{ position:absolute; top:0; left:0; width:1280px; height:768px; border-collapse:collapse; }
        .divider {background-color:#febc11; height:5px; line-height:5px;}
        .hide {display:none; height:0;}
        /*
        .righty tr td:nth-child(1), .righty tr td:nth-child(4) {width:40%;}
        .righty tr td:nth-child(2), .righty tr td:nth-child(5) {width:6%;}
        .righty tr td:nth-child(3) {width:8%;}

        .bottom {border-collapse:collapse; width:100%;}
        .bottom tr td:nth-child(1) {width:63%; color:#c3a385; font-size:20px; font-family: "Avenir Light";}
        .bottom tr td:nth-child(2) {width:37%; color:#775e4b; font-size:17px; text-align:right; font-family: "Avenir Light";}
        */
    </style>
    <table id="table1">
        <tbody>
        <tr>
            <td class="left-side"><!--video autoplay="" loop=""><source src="EBBM41920Hot.ogv" /><source src="http://vi-digital.com/lib/vicorporatemenus/Corporate---EBB/EBBM41280Hot.ogv" /></video-->
                <table class="main" style="top: 195px;">
                    <tbody>
                    <tr>
                        <td class="item-up">Nova Lox <span class="cal">480 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Nova Lox*, Red Onion, Capers, Tomato with Plain Shmear on a Plain Bagel</td>
                    </tr>
                    <tr>
                        <td class="item-up">Tasty Turkey <span class="cal">500 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Spinach, Cucumber, Lettuce, Tomato with Onion &amp; Chive Shmear</td>
                    </tr>
                    <tr>
                        <td class="item-up">Turkey, Bacon &amp; Avocado <span class="cal">670 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">on a Honey Whole Wheat Begal</td>
                    </tr>
                    <tr>
                        <td class="item-up">Hummus Veg Out <span class="cal">420 Cal</span></td>
                        <td class="price" rowspan="2">6.49</td>
                    </tr>
                    <tr>
                        <td class="desc">Hummus, Tomato, Red Onion, Spinach, Lettuce, roasted Red Peppers, Cucumber with Garden Veggie Shmear on a Sesame Seed Bagel</td>
                    </tr>
                    <tr>
                        <td class="item-up">Turkey Club Mex Wrap <span class="cal">740 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Applewood Bacon, Pepper Jack Cheese, Lettuce, Tomato, Red Onion, Spinach with Ancho Mayo on a Whole Wheat Tortilla</td>
                    </tr>
                    <tr>
                        <td class="item-up">Thintastic&trade; Club <span class="cal">400 Cal</span></td>
                        <td class="price" rowspan="2">4.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Applewood Bacon, Avocado, Lettuce, Tomato with Roasted Tomato Spread on a Honey Whole Wheat Thintastic&trade; Bagel</td>
                    </tr>
                    <tr>
                        <td class="header" colspan="2">Deli <span class="header-sup">Served on your choice of Multigrain Bread, Bagel or Wheat Tortilla</span></td>
                    </tr>
                    <tr>
                        <td class="divider" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="item">Turkey &amp; cheddar <span class="cal">540-670 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Harvest Chicken Salad <span class="cal">460-590 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Albacore Tuna Salad <span class="cal">460-590 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Ham &amp; Swiss <span class="cal">540-670 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="cal" colspan="2">* Cold smoked salmon is not cooked. Consuming raw or under cooked seafood may increase the risk of foodborne illness.</td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="middle">&nbsp;</td>
            <td class="right-side"><!--video autoplay="" loop=""><source src="EBBM41920Sig.ogv" /><source src="http://vi-digital.com/lib/vicorporatemenus/Corporate---EBB/EBBM41280Sig.ogv" /></video-->
                <table class="main righty" style="top: 2px; height: 426px;">
                    <tbody>
                    <tr>
                        <td class="item-up">Italian Chicken Tostini&reg; <span class="cal">660 Cal</span></td>
                        <td class="price" rowspan="2">7.69</td>
                    </tr>
                    <tr>
                        <td class="desc">Grilled Chicken Breast, Pepperoni, Spinach, Roasted Red Peppers, Mozzarella Cheese with Basil Pesto on Ciabatta Bread</td>
                    </tr>
                    <tr>
                        <td class="item-up">Turkey Club Tostini&reg; <span class="cal">760 Cal</span></td>
                        <td class="price" rowspan="2">7.69</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Applewood Bacon, Spinach, Tomato, Mozzarella Cheese with Roasted Tomato Spread on Ciabatta Bread</td>
                    </tr>
                    <tr class="hide">
                        <td class="item-up">Buffalo Chicken &amp; bacon Tostini&reg; <span class="cal">570 Cal</span></td>
                        <td class="price" rowspan="2">0.00</td>
                    </tr>
                    <tr class="hide">
                        <td class="desc">Grilled Chicken Breast, Applewood Bacon, Mozzarella Cheese, Frank&#39;s&reg; RedHot&reg; Sauce and Red Onions on Ciabatta Bread</td>
                    </tr>
                    <tr>
                        <td class="item-up">BBQ Chicken Tostini&reg; <span class="cal">560 Cal</span></td>
                        <td class="price" rowspan="2">5.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Grilled Chicken Breast, BBQ Sauce, Mozzarella Cheese and Red Onions on Ciabatta Bread</td>
                    </tr>
                    <tr>
                        <td class="item-up">Roasted Veggie Tostini&reg; <span class="cal">560 Cal</span></td>
                        <td class="price" rowspan="2">5.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Asparagus, Sauteed Mushrooms, Spinach, Roasted Red Peppers, Balsamic Onions, Mozzarella Cheese with Garlic &amp; Herb Shmear on Ciabatta Bread</td>
                    </tr>
                    <tr class="hide">
                        <td class="item-up">Cheesy Chicken &amp; Asparagus Melt <span class="cal">440 Cal</span></td>
                        <td class="price" rowspan="2">0.00</td>
                    </tr>
                    <tr class="hide">
                        <td class="desc">Grilled Chicken Breast, Three Cheese Blend, Roasted Asparagus and Balsamic Onions on a Plain Thintastic&trade;</td>
                    </tr>
                    <tr class="hide">
                        <td class="item-up">Ham &amp; Swiss Melt <span class="cal">350 Cal</span></td>
                        <td class="price" rowspan="2">0.00</td>
                    </tr>
                    <tr class="hide">
                        <td class="desc">Ham, Swiss Cheese and Tomato on a Plain Bagel</td>
                    </tr>
                    <tr class="hide">
                        <td class="item-up">Turkey &amp; Cheddar Melt <span class="cal">430 Cal</span></td>
                        <td class="price" rowspan="2">0.00</td>
                    </tr>
                    <tr class="hide">
                        <td class="desc">Roasted Turkey, Cheddar Cheese and Tomato on a Plain Bagel</td>
                    </tr>
                    <tr>
                        <td class="item-up">Thintastic&trade; Buffao Chicken <span class="cal">410 Cal</span></td>
                        <td class="price" rowspan="2">4.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Grilled Chicken Breast, Frank&#39;s&reg; HotRed&reg; Sauce, Lettuce Tomato, Red Onion with Reduced Fat Plain Shmear on a Plain Thintastic&trade; Bagel</td>
                    </tr>
                    <tr>
                        <td class="item-up">Pizza Bagel: Cheese <span class="desc">on a Plain Bagel</span> <span class="cal">440 Cal</span></td>
                        <td class="price">5.59</td>
                    </tr>
                    <tr>
                        <td class="item-up">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Pepperoni <span class="desc">on a Plain Bagel</span> <span class="cal">530 Cal</span></td>
                        <td class="price">5.59</td>
                    </tr>
                    <tr>
                        <td class="divider" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="width:100%;">
                                <tbody>
                                <tr>
                                    <td class="header">turkey chili</td>
                                    <td class="cal">260 Cal (bowl)</td>
                                    <td class="price" style="text-align:left;">3.59</td>
                                    <td class="cal" style="padding-left: 7px; text-align:center;">160 Cal (cup)</td>
                                    <td class="price">2.59</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="cal" colspan="2">2,000 calories a day is used for general nutrition advise, but calorie needs vary. Written nutrition information available upon request</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- end fix --></div>
