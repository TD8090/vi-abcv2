<div id="vi-fix" style="position: relative; top: 0px; left: 0px;">
    <style type="text/css">@font-face {
            font-family: "Avenir";
            src:  url(Avenir-Book.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Book.ttf) format("truetype");
        }
        @font-face {
            font-family: "Avenir Light";
            src:  url(Avenir-Light.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Light.ttf) format("truetype");
        }
        @font-face {
            font-family: "Avenir Medium";
            src:  url(Avenir-Medium.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Medium.ttf) format("truetype");
        }
        @font-face {
            font-family: "Avenir Heavy";
            src:  url(Avenir-Heavy.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Heavy.ttf) format("truetype");
        }
        @font-face {
            font-family: "Avenir Black";
            src:  url(Avenir-Black.ttf) format("truetype"), url(http://vi-digital.com/lib/fonts/Avenir/Avenir-Black.ttf) format("truetype");
        }

        /*
        DIN Bold did look good, but they're using Avenir now
        @font-face {
            font-family: "DIN Bold";
            src:  url(din_bold-webfont.svg) format("svg"), url(http://vi-digital.com/lib/pjjdent/din_bold-webfont.svg) format("svg"), url(http://vi-digital.com/lib/fonts/DIN_Bold.ttf) format("truetype");
        }*/

        body {
            background-color:#FFFFFF;
            border:none;
            color:#4a3728;
            margin:0;
            font-family: "Avenir", sans-serif;
            -webkit-font-smoothing: antialiased;
        }
        table, td {
            font-family: "Avenir Medium", sans-serif;
            font-size: 22px; vertical-align:top;
            text-align: left;
        }
        table {border-collapse:collapse; position:absolute;}
        .middle {background-color:#4a3728; width:4px;}
        .left-side, .right-side {width:100%; height:auto;}
        .left-side, .right-side {text-align:center; width:49.7%;}
        .header     {color:#febc11;font-size:40px; font-weight:bold; font-family: "Avenir Heavy"; text-transform:uppercase;}
        .header-sm  {color:#febc11;font-size:32px; font-weight:bold; font-family: "Avenir Heavy"; text-transform:capitalize;}
        .header-desc {color:#775e4b; text-transform:none; font-size:25px; font-weight:bold; font-family: "Avenir Light";}
        .upp{text-transform:uppercase;}
        .cap{text-transform:capitalize;}
        .icon-sm{padding-bottom: 0; padding-left: 2px; padding-right: 2px; height: 15px; width:auto; padding-top: 3px}
        .icon-md{padding-bottom: 0; padding-left: 2px; padding-right: 2px; height: 24px; width:auto; padding-top: 3px}
        .item-up {text-transform:uppercase;}
        .item { text-transform:capitalize;}
        .price { text-align:right;}
        .item-up, .item, .price {color:#4a3728; font-size:26px;  font-family: "Avenir Black"; letter-spacing:1px;	-webkit-font-smoothing: antialiased;}

        .desc {color:#775e4b; font-size:24px; font-family: "Avenir Light"; font-weight:bold;}
        .cal {color:#c3a385; font-size:25px; text-transform:Capitalize; font-family: "Avenir Light";}
        .foot-iconlegend{}
        .foot-coldsmokedwarning{color:#c3a385; font-size:25px; text-transform:Capitalize; font-family: "Avenir Light";}
        .foot-caladvice{}
        .foot-nutella{}

        table table.main {width:98%; height:766px; margin:0 auto; text-align:left;}

        .divider {background-color:#febc11; height:5px; line-height:5px;}

        .righty tr td:nth-child(1), .righty tr td:nth-child(4) {width:42%;}
        .righty tr td:nth-child(2), .righty tr td:nth-child(5) {width:6%;}
        .righty tr td:nth-child(3) {width:4%;}

        .bottom {border-collapse:collapse; width:100%;}
        .bottom tr td:nth-child(1) {width:63%; color:#c3a385; font-size:20px; font-family: "Avenir Light";}
        .bottom tr td:nth-child(2) {width:37%; color:#775e4b; font-size:17px; text-align:right; font-family: "Avenir Light";}
    </style>
    <table class="" style="position:absolute; top:0; left:0; width:1920px; height:1080px; border-collapse:collapse;">
        <tbody>
        <tr>
            <td class="left-side"><!--video autoplay="" loop=""><source src="EBBM41920Bfast.ogv" /><source src="http://vi-digital.com/lib/vicorporatemenus/Corporate---EBB-HDMI/EBBM41920Bfast.ogv" /></video-->
                <table class="tbl-left" style="top: 294px;">
                    <tbody>
                    <tr>
                        <td class="item upp">Nova Lox <span class="cal">480 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Nova Lox*, Red Onion, Capers, Tomato with Plain Shmear on a Plain Bagel</td>
                    </tr>
                    <tr>
                        <td class="item upp">Tasty Turkey <span class="cal">500 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Spinach, Cucumber, Lettuce, Tomato with Onion &amp; Chive Shmear</td>
                    </tr>
                    <tr>
                        <td class="item upp">Turkey, Bacon &amp; Avocado <span class="cal">670 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">on a Honey Whole Wheat Begal</td>
                    </tr>
                    <tr>
                        <td class="item upp">Hummus Veg Out <span class="cal">420 Cal</span></td>
                        <td class="price" rowspan="2">6.49</td>
                    </tr>
                    <tr>
                        <td class="desc">Hummus, Tomato, Red Onion, Spinach, Lettuce, roasted Red Peppers, Cucumber with Garden Veggie Shmear on a Sesame Seed Bagel</td>
                    </tr>
                    <tr>
                        <td class="item upp">Turkey Club Mex Wrap <span class="cal">740 Cal</span></td>
                        <td class="price" rowspan="2">6.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Applewood Bacon, Pepper Jack Cheese, Lettuce, Tomato, Red Onion, Spinach with Ancho Mayo on a Whole Wheat Tortilla</td>
                    </tr>
                    <tr>
                        <td class="item upp">Thintastic&trade; Club <span class="cal">400 Cal</span></td>
                        <td class="price" rowspan="2">4.99</td>
                    </tr>
                    <tr>
                        <td class="desc">Roasted Turkey, Applewood Bacon, Avocado, Lettuce, Tomato with Roasted Tomato Spread on a Honey Whole Wheat Thintastic&trade; Bagel</td>
                    </tr>
                    <tr>
                        <td class="header" colspan="2">Deli <span class="header-desc">Served on your choice of Multigrain Bread, Bagel or Wheat Tortilla</span></td>
                    </tr>
                    <tr>
                        <td class="divider" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="item">Turkey &amp; cheddar <span class="cal">540-670 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Harvest Chicken Salad <span class="cal">460-590 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Albacore Tuna Salad <span class="cal">460-590 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Ham &amp; Swiss <span class="cal">540-670 Cal</span></td>
                        <td class="price">6.49</td>
                    </tr>
                    </tbody>
                </table>
                <table id="warning">
                    <tr>
                        <td class="bottom">* Cold smoked salmon is not cooked. Consuming raw or under cooked seafood may increase the risk of foodborne illness.</td>
                        <td class="item-up" style="text-align:right; ">
                            <img src="http://vi-digital.com/lib/pjjdent/EBB_icon2_leaf.png" class="icon-md" />&nbsp;Vegetarian<br/>
                            <img src="http://vi-digital.com/lib/pjjdent/EBB_icon2_nut.png" class="icon-md" />&nbsp;Contains Nuts
                        </td>
                    </tr>
                </table>
            </td>
            <td class="middle">&nbsp;</td>
            <td class="right-side"><!--video autoplay="" loop=""><source src="EBBM41920Bagel.ogv" /><source src="http://vi-digital.com/lib/vicorporatemenus/Corporate---EBB-HDMI/EBBM41920Bagel.ogv" /></video-->
                <table class="main righty">
                    <tbody>
                    <tr>
                        <td class="item">Bagel <span class="cal">260-320 Cal</span></td>
                        <td class="price">1.19</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item">power protein bagel <span class="cal">350 Cal</span></td>
                        <td class="price">0.60</td>
                    </tr>
                    <tr>
                        <td class="divider" colspan="5">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="header" colspan="2">Bagel + Topping</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="header" colspan="2">Bagel Boxes</td>
                    </tr>
                    <tr>
                        <td class="item">Shmear <span class="cal">390-470 Cal</span></td>
                        <td class="price">2.69</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item">Baker&#39;s Dozen Box</td>
                        <td class="price">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="item">Nutella&reg; <span class="cal">490-550 Cal</span></td>
                        <td class="price">1.79</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="desc">13 Bagels &amp; 2 Shmear Tubs</td>
                        <td class="price">14.99</td>
                    </tr>
                    <tr>
                        <td class="item">Hummus <span class="cal">360-420 Cal</span></td>
                        <td class="price">2.69</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="desc">Bagels Only</td>
                        <td class="price">10.99</td>
                    </tr>
                    <tr>
                        <td class="item">Peanut Butter <span class="cal">430-490 Cal</span></td>
                        <td class="price">1.79</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item">Half Dozen Box</td>
                        <td class="price">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="item">PB&amp;J <span class="cal">500-560 Cal</span></td>
                        <td class="price">2.69</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="desc">6 Bagels &amp; 1 Shmear Tub</td>
                        <td class="price">8.99</td>
                    </tr>
                    <tr>
                        <td class="item">Butter Blend <span class="cal">440-500 Cal</span></td>
                        <td class="price">1.79</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="desc">Bagels Only</td>
                        <td class="price">6.49</td>
                    </tr>
                    <tr>
                        <td class="item">Honey Butter <span class="cal">410-470 Cal</span></td>
                        <td class="price">1.79</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item">Extra Tub of Shmear</td>
                        <td class="price">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="item">&nbsp;</td>
                        <td class="price">&nbsp;</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="cal">520-600 Cal</td>
                        <td class="price">3.59</td>
                    </tr>
                    <tr>
                        <td class="header" colspan="5">Double-Whipped Shmear</td>
                    </tr>
                    <tr>
                        <td class="header-sm" colspan="2">Regular</td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="header-sm" colspan="2">Reduced Fat*</td>
                    </tr>
                    <tr>
                        <td class="item" colspan="2">Plain <span class="cal">150 Cal</span></td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item" colspan="2">Reduced Fat Plain <span class="cal">130 Cal</span></td>
                    </tr>
                    <tr>
                        <td class="item" colspan="2">Onion &amp; Chive <span class="cal">140 Cal</span></td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item" colspan="2">Honey Almond <span class="cal">150 Cal</span></td>
                    </tr>
                    <tr>
                        <td class="item" colspan="2">Smoked Salmon <span class="cal">130 Cal</span></td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item" colspan="2">Strawberry <span class="cal">140 Cal</span></td>
                    </tr>
                    <tr>
                        <td class="item" colspan="2">Jalape&ntilde;o Salsa <span class="cal">130 Cal</span></td>
                        <td class="mid-middle">&nbsp;</td>
                        <td class="item" colspan="2">Garden Veggie <span class="cal">130 Cal</span></td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table class="bottom">
                                <tbody>
                                <tr>
                                    <td class="leg-left">*25% less fat than our regular shmear.<br />
                                        Fat content has been reduced from 7g to 5g per serving.</td>
                                    <td class="font-tm">Nutella&reg; is a registered trademark of<br />
                                        Ferrero S.p.A. All rights reserved.</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table></div>
