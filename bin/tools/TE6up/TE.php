<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>Template Previewer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="stuff/src/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="stuff/TE_styles.css" rel="stylesheet">
    <style>
        #viheader{
            position: fixed;right: 0;width: 240px;cursor:default;z-index:-1000;
            color: rgba(255, 165, 0, 1);font: bolder 24px "EurostileExt2" ;
            animation: slidein 4s;text-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1);
            -moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;
        }@keyframes slidein {from {color: rgba(0, 0, 0, 0); margin-right: 100%;}to {color: rgba(255, 165, 0, 1); margin-right: 0;}}
        .iframe{  margin: 0 30px;  }
    </style>
</head>

<body>
<div id="viheader" unselectable="on" onselectstart="return false;" onmousedown="return false;">
    Vi Templater</div>
<?php
function makelink($path, $text){ return <<<HTML
<a href="$path" style="position: relative; line-height:16px; font-size:20px; text-decoration:none;color:#ffffff;">
<span style="position:absolute; top:4px; left:45px; -webkit-transform:scale(2,1);background: url('stuff/src/space.jpg'); background-size:cover; border:3px solid black;">$text</span></a><br/>
HTML;
}
include 'stuff/TE_classes.php';
$wh1 = 'width="1280" height="768"';
$wh2 = 'width="1920" height="1080"';
$rgx_vid = "/<(!--)(video ([\s\S]*?)<\\/video)(--)>/";
$rgx_vid_sub = "<$2>";
for($i=1;$i<=6;$i++){
    $Tmp[$i-1] = file_get_contents("TE_".$i.".php");
    $Tmphf[$i-1] =  $thead.$Tmp[$i-1].$tfoot;
    $Tmpresult[$i-1] = preg_replace($rgx_vid, $rgx_vid_sub, $Tmphf[$i-1]);
    file_put_contents("stuff/TE_ifr".$i.".html", $Tmpresult[$i-1]);
}
echo makelink("stuff/TE_ifr1.html", "T1").'<iframe class="iframe" src="stuff/TE_ifr1.html" '.$wh1.'></iframe><br/>';
echo makelink("stuff/TE_ifr2.html", "T2").'<iframe class="iframe" src="stuff/TE_ifr2.html" '.$wh1.'></iframe><br/>';
echo makelink("stuff/TE_ifr3.html", "T3").'<iframe class="iframe" src="stuff/TE_ifr3.html" '.$wh1.'></iframe><br/>';
echo makelink("stuff/TE_ifr4.html", "T4").'<iframe class="iframe" src="stuff/TE_ifr4.html" '.$wh1.'></iframe><br/>';
echo makelink("stuff/TE_ifr5.html", "T5").'<iframe class="iframe" src="stuff/TE_ifr5.html" '.$wh1.'></iframe><br/>';
echo makelink("stuff/TE_ifr6.html", "T6").'<iframe class="iframe" src="stuff/TE_ifr6.html" '.$wh1.'></iframe><br/>';
?>
</body>
</html>