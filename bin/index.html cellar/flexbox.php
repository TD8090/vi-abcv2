<!DOCTYPE HTML><head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
<!--    <link href="css/lightbox-carousel.css" type="text/css" rel="stylesheet">-->
    <style type="text/css">
        * {margin: 0;padding: 0; box-sizing: border-box;}
        body {          }
        #viewport {
            width: 1280px;
            height:768px;
            margin: 0 auto;
            border:1px solid #000;
            background:#fff;
            overflow:auto;
            display:flex;
            flex-direction: row;
//             flex-direction: column;
            flex-wrap: wrap;
        }

        .box {
            display: inline-block;
            width:auto;
            height: auto;
//             min-width:60px;
//             min-height:70px;
            margin:2px;
            border:1px solid #f00;
            box-sizing: border-box;
            text-align:center;
            float:left;
            flex: 1; 
            flex-basis:12%;
        }
    </style>
</head>


<body>

<?php
$w = 1280;$h = 768;$num_items = 70;
echo '<div id="fitandcenter">';
    echo '<div id="viewport">';
        for($i=1;$i<=$num_items;$i++){
            echo '<div class="box"></div>';
        }
    echo '</div>';
echo '</div>';
?>

</body>


<script src="js/jquery-1.11.2.min.js"></script>
<!--<script src="js/freewall.js"></script>-->
<!--<script src="js/lightbox-carousel.js" type="text/javascript"></script>-->
<script type="text/javascript">

    $(document).ready(function(){

        var module_getjson = (function() {
            var module_runjson = function(data) {
                jQuery.each(data, function(key, val){
                    var thisbox = $("<div />");
                    var thisimg = $("<img />");
                    thisbox.addClass("cell " + val.id).appendTo("#thumbwall");
                    thisimg.attr("src", val.url_image).addClass("item-img").appendTo(thisbox);

                    /*
                     var thatbox = $("<div />");
                     var thatimg = $("<img />");
                     thatbox.addClass("carousel-main").appendTo(".main");
                     thatimg.attr({
                     src: val.url_image,
                     title: "Image" + val.name
                     }).appendTo(thatbox);
                     */
                });
                //create an EVENT and dispatch now because the divs are finished
                var ev_nodes_inserted = document.createEvent('Event');
                ev_nodes_inserted.initEvent('ev_nodes_inserted', true, true);
                document.dispatchEvent(ev_nodes_inserted);

                var arr_len;
                var arr = [];
                for(key in data) {
                    arr.push(key);  }
                arr_len = arr.length;

                var exports = {};
                exports.num_items = arr_len;
                console.log(exports.num_items);
                return exports.num_items;
            };
//        console.log(module_runjson.exports);
            $.getJSON('js/json_items.json', module_runjson );
//        console.log(num_items);
        })();//end jsonmod


//        var num_items = jsonmod.num_items;
//        console.log(num_items);
//        console.log(jsonmod.num_items);

//            return {
//                num_items_method: function () {
//                    var num_items;
//                    num_items = arr_len;
//                };

        /*CUSTOM LOGS*//*

         (function() {
         var num_items = $.data(document, "num_items");
         var logcontainer = document.getElementById('logcontainer');
         var numlogs = 1;
         var logid = [];
         // make 'logdiv's with numbered IDs for innerHTML fills below
         for (var j = 1; j <= numlogs; j++) {
         var newlogdiv = document.createElement('div');
         var divIdName = 'logdiv' + j;
         newlogdiv.setAttribute('id', divIdName);
         logcontainer.appendChild(newlogdiv);
         logid[j] = document.getElementById("logdiv" + j);
         console.log(logid[1]);
         }
         logid[1].innerHTML = 'num_items: '        + num_items;
         //            logid[2].innerHTML = 'uratiowidth: '    + uratiowidth;
         //            logid[3].innerHTML = 'uratioheight: '   + uratioheight;
         //            logid[4].innerHTML = 'optcellarea: '    + optcellarea;
         //            logid[5].innerHTML = 'optsquarewh: '    + optsquarewh;
         //            logid[6].innerHTML = 'optx: '           + optx;
         //            logid[7].innerHTML = 'optrectwidth: '   + optrectwidth;
         //            logid[8].innerHTML = 'optrectheight: '  + optrectheight;
         //            logid[9].innerHTML = ': ' + optcellarea;
         })();//end custom logs
         */


        // Listen for the event.
        document.addEventListener('ev_nodes_inserted', function (e) {
            // e.target matches document from above
            $(function(){

                var wall = new freewall("#freewall");
                wall.reset({
                    selector: '.cell',
//                    animate: true,
//                    cellW: 60,
//                    cellH: 40,
                    fixSize: 1,
                    gutterX: 2,
                    gutterY: 2,
                    onResize: function() {
                        wall.fitZone(1280, 768);
                    }
                });
                wall.fitZone(1280, 768);
                $(window).trigger("resize");


                /*
                 $(".main").lightboxCarousel({
                 baseClass: ".carousel-main",
                 parentClass: ".main",
                 totalImages: 10,
                 activeImageIndex: 10,
                 imgTopOffset : 0,
                 divTopPadding: 40,
                 divLeftPadding: 20,
                 maxWidth: 600,
                 maxHeight: 600
                 });
                 */
            })
        }, false);


    });/*end:doc ready*/
</script>

