<?php
function h4v($val){return '<div id="'.$val.'" class="btn-group-vertical btn-group-xs" data-toggle="buttons"><h4>'.$val.'</h4>';}
function h4h($val){return '<div id="'.$val.'" class="btn-group btn-group-xs" data-toggle="buttons"><h4>'.$val.'</h4>';}
function o($val){return '<label class="btn btn-warning"><input type="radio" name="options" value="'.$val.'">'.$val.'</label>';}
function oc($val, $class1){return '<label class="btn btn-warning '.$class1.'"><input type="radio" name="options" value="'.$val.'">'.$val.'</label>';}
function otc($val, $txt, $class1){return '<label class="btn btn-warning '.$class1.'"><input type="radio" name="options" value="'.$val.'">'.$txt.'</label>';}
$panel = '<div class="btn-toolbar btn-group-sm panel panel-warning">';$cdiv = '</div>';
$panelsuc = '<div class="btn-toolbar btn-group-sm panel panel-success">';$cdiv = '</div>';

echo '<div id="changeling-panel">'
, $panel, h4v("flex-direction"), o("row"), o("row-reverse"), o("column"), o("column-reverse"), $cdiv
, h4v("flex-wrap"), o("nowrap"), o("wrap"), o("wrap-reverse"), $cdiv, $cdiv
, $panel, h4v("justify-content"), o("flex-start"), o("flex-end"), o("center"), o("space-between"), o("space-around"), $cdiv, $cdiv
, $panel, h4v("align-content"), o("stretch"), o("flex-start"), o("flex-end"), o("center"), o("space-between"), o("space-around"), $cdiv, $cdiv
, $panel, h4v("align-items"), o("stretch"), o("flex-start"), o("flex-end"), o("center"), o("baseline"), $cdiv, $cdiv;
?>

<div class="btn-toolbar btn-group-sm panel panel-success">
    <div id="items" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>items</h4>
        <input type="number" class="btn btn-xs" id="numi" style="width:70px; height:19px;" title="numi"/>
        <button class="btn btn-xs btn-info" id="updatenumi">Update</button>
        <button class="btn btn-xs btn-primary w50" id="removecell">ITEM-</button>
        <button class="btn btn-xs btn-primary w50" id="addcell">ITEM+</button>
    </div>
    <div id="height" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>height</h4>
        <?php
        otc("auto","auto","w25");
        otc("100%","100%","w25");
        otc("500px","500","w25");
        otc("inherit","inher","w25");
        ?>
    </div>
    <div id="width" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>width</h4>
        <label class="btn btn-warning w25"><input type="radio" name="options" value="auto">auto</label>
        <label class="btn btn-warning w25"><input type="radio" name="options" value="100%">100%</label>
        <label class="btn btn-warning w25"><input type="radio" name="options" value="initial">init</label>
        <label class="btn btn-warning w25"><input type="radio" name="options" value="inherit">inher</label>
    </div>

    <div id="max-height" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>max-height</h4>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="none">none</label>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="inherit">inher</label>
    </div>

    <div id="max-width" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>max-width</h4>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="none">none</label>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
        <label class="btn btn-warning w33"><input type="radio" name="options" value="inherit">inher</label>
    </div>


</div>
<?php
echo $panelsuc
, h4v("align-self"), o("auto"), o("flex-start"), o("flex-end"), o("center"), o("baseline"), $cdiv
, h4h("flex-grow"), oc("0", "w50"), oc("1", "w50"), $cdiv
, h4h("flex-shrink"), oc("1", "w50"), oc("0", "w50"), $cdiv
, $cdiv;

?>
<div class="btn-toolbar btn-group-sm panel panel-success text-center">
    <h4>flex-basis</h4>
    <button id="poptip_flexbasis" type="button" class="btn-poptip" data-container="body"
            data-toggle="popover" data-placement="bottom" data-trigger="focus" >?</button>

    <div id="fbasisradios" class="btn-group-xs" data-toggle="buttons">
    </div>

</div>
<div class="btn-toolbar btn-group-sm panel panel-success text-center">
    <div id="matchfbdiv" class="btn-group btn-group-xs" data-toggle="buttons">
        <h4>spec-ops</h4>
        <button class="btn btn-xs btn-primary" id="matchfb">Match Flex-basis</button>
    </div>
</div>
</div>
