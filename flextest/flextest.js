//====================
//=========flex-parent
//  flex-direction              //  flex-wrap         //  align-content
//    ^row                      //    ^nowrap         //    ^stretch            //========flex-children
//    row-reverse               //    wrap            //    flex-start          //order         0
//    column                    //    wrap-reverse    //    flex-end            //flex-grow     0
//    column-reverse                                  //    center              //flex-shrink   1
//                                                    //    space-between       //flex-basis    auto
//  justify-content             //  align-items       //    space-around        //align-self
//    ^flex-start               //    ^stretch                                  //    ^auto
//    flex-end                  //    flex-start      //flex:1                  //    flex-start
//    center                    //    flex-end                                  //    flex-end
//    space-between             //    center                                    //    center
//    space-around              //    baseline                                  //    baseline
//    https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties

gojira = {
    'config':function(){
        this.sandbox = $("#sandbox");
        this.colspin = $('#col-spin');
        this.numspin = $('#num-spin');
        this.initShow = 33;
        this.initTotal = 100;
    }
    , 'init':function(){
        this.config();
        this.initCells(this.initShow,this.initTotal);
        this.initActiveDefaults();
        this.styleStuff();
        this.listen();
        $(window).load(function(){gojira.showDimDiffs();})
    }
    , 'listen':function(){
        var x = this;
        $("#num-spin").on("change", function (e) {
            x.toggleCells($(this).val());
            x.styleStuff();
            x.showDimDiffs();
        });
        $('div[data-toggle="buttons"]').on('change', function (e) {
            x.styleStuff();
            x.showDimDiffs();
        });
        $('#getdimdiffs').on("click", function (e) {
            e.stopPropagation();
            x.showDimDiffs();
        });
        $('#matchfb').on("click", function (e) {
            var jstyle = $("#jstyle");
            var curwid = $('#sandbox').find('.cell:first-child').find('img')[0].offsetHeight;
            var restyle = jstyle.html().replace(/flex-basis:(.*);?/, 'flex-basis:' + curwid.toString() + 'px;');
            jstyle.html(restyle);
        });
        $('#match-heights').on("click", function (e) {
            x.matchHeights();
        });
        $('#organize-cells').on("click", function (e) {
            x.organizeCells();
            x.showDimDiffs();
        });
        $(document).on('keydown', function(e){
            x.interceptArrowKeys(e);
        });
    }
    , 'organizeCells':function() {
        var objs = $('.cell.show');
        console.log(objs[0].firstChild.offsetWidth);
        //$(objs).each(function (idx) {
        //    console.log('ea', this, idx);
        //});
        function owsortcomparator(a,b) {
            return parseInt(a.firstChild.naturalWidth, 10) - parseInt(b.firstChild.naturalWidth, 10);
        }
        objs.sort(owsortcomparator);
        console.log(objs[0].firstChild.offsetWidth);
        this.sandbox.remove('.cell.show').prepend(objs);
    }

    , 'interceptArrowKeys':function(evt) {
        var x = this
        ,keycode = (evt.keyCode ? evt.keyCode : evt.which)
        ,curfb = x.colspin.val()
        ,curnm = x.numspin.val();
        if(keycode == '37'){      //left
            evt.preventDefault();evt.stopPropagation();
            if(curfb > 1) x.colspin.val(parseInt(curfb) - 1).trigger('change');
        }else if(keycode == '39'){//right
            evt.preventDefault();evt.stopPropagation();
            if(curfb < 99) x.colspin.val(parseInt(curfb) + 1).trigger('change');
        }else if(keycode == '40'){//down
            evt.preventDefault();evt.stopPropagation();
            if(curnm > 1) x.numspin.val(parseInt(curnm) - 1).trigger('change');
        }else if(keycode == '38'){//up
            evt.preventDefault();evt.stopPropagation();
            if(curnm < 99) x.numspin.val(parseInt(curnm) + 1).trigger('change');
        }
    }

    , 'matchHeights':function() {
        var jstyle = $("#jstyle");
        var lowheight = 5000;
        $('.cell.show img').each(function () {
            if($(this)[0].offsetHeight < lowheight){
                lowheight = this.offsetHeight;
            }
        });
        var restyle = jstyle.html().replace(/(?:.cell img{.|\n|\r)*?height: (.*?);/
            , 'height:' + lowheight.toString() + 'px;');
        jstyle.html(restyle);
        this.showDimDiffs();
    }

    , 'showDimDiffs':function(){//show popover, +/- dratio/dsize,
        var rnat, roff, rdiff, hdiff, wdiff;
        $('.cell.show').each(function (idx) {
            var $cell=$(this)
                ,img=$cell.find('img')[0]
                ,wid=$cell.find('.wid')
                ,hei=$cell.find('.hei')
                ,rat=$cell.find('.rat');
            rnat = (img.naturalWidth / img.naturalHeight).toFixed(4);
            roff = (img.offsetWidth / img.offsetHeight).toFixed(4);
            rdiff = (rnat - roff).toFixed(3);
            wdiff = (img.offsetWidth-img.naturalWidth);
            hdiff = (img.offsetHeight-img.naturalHeight);
            if (Math.abs(hdiff) > 0) {
                hei.text('H:'+img.offsetHeight);
            }else{hei.text('');}
            if (Math.abs(wdiff) > 0) {
                wid.text('W:'+img.offsetWidth+'('+wdiff+')');
            }else{wid.text('');}
            if (Math.abs(rdiff) > .02) {
                rat.text('R:'+rdiff);
            }else{rat.text('');}
        });
    }


    , 'randInt':function(min, max){return min + Math.floor(Math.random() * (max - min + 1));}
    , 'randColorRgb':function(){return 'rgb(' + Math.floor(Math.random() * 255) + ','
                + Math.floor(Math.random() * 255) + ',' + Math.floor(Math.random() * 255) + ')';}
    , 'randColorHex':function(){return "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});}

    , 'initCells':function(show,max){//** B U I L D ** cell>imgs to sandbox, .show x10
        for (var a = 1; a <= max; a++) {
            var cdiv = $('<div class="cell">').attr('style','border-color:'+this.randColorHex()+';');
            var im = $('<img />').attr('src','src/testnums/'+this.randInt(1,100)+'.png');
            im.appendTo(cdiv);
            var diffs = $('<div class="diffs">');
            $('<div class="hei">').appendTo(diffs);
            $('<div class="wid">').appendTo(diffs);
            $('<div class="rat">').appendTo(diffs);
            diffs.appendTo(cdiv);
            cdiv.appendTo(this.sandbox);
        }
        this.numspin.val(show);

        this.cells = $('.cell');
        this.cells.slice(0,show).addClass('show');
    }

    , 'toggleCells':function(num){
        this.cells.slice(0,num).addClass('show');
        this.cells.slice(num).removeClass('show');
    }

    , 'initActiveDefaults':function(){
        /* make default .active styles */
        $('#flex-direction').find('label').has('input[value="row"]').addClass('active');
        $('#flex-wrap').find('label').has('input[value="wrap"]').addClass('active');
        $('#justify-content').find('label').has('input[value="space-around"]').addClass('active');
        $('#align-content').find('label').has('input[value="stretch"]').addClass('active');
        $('#align-items').find('label').has('input[value="stretch"]').addClass('active');
        $('#height').find('label').has('input[value="auto"]').addClass('active');
        $('#width').find('label').has('input[value="auto"]').addClass('active');
        $('#max-height').find('label').has('input[value="100%"]').addClass('active');
        $('#max-width').find('label').has('input[value="100%"]').addClass('active');
        $('#align-self').find('label').has('input[value="auto"]').addClass('active');
        $('#flex-grow').find('label').has('input[value="1"]').addClass('active');
        $('#flex-shrink').find('label').has('input[value="1"]').addClass('active');
    }
    , 'styleStuff':function(){
        //grab all settings from active tags
        var fdir = $('#flex-direction').find('label.active>input').val();
        var fwra = $('#flex-wrap').find('label.active>input').val();
        var fjst = $('#justify-content').find('label.active>input').val();
        var falc = $('#align-content').find('label.active>input').val();
        var fali = $('#align-items').find('label.active>input').val();
        var fals = $('#align-self').find('label.active>input').val();
        var fgro = $('#flex-grow').find('label.active>input').val();
        var fshr = $('#flex-shrink').find('label.active>input').val();
        var ihei = $('#height').find('label.active>input').val();
        var iwid = $('#width').find('label.active>input').val();
        var imxh = $('#max-height').find('label.active>input').val();
        var imxw = $('#max-width').find('label.active>input').val();
        var fsuf = $('#fbasis').find('label.active>input').val();
        var fnum = $('#col-spin').val();
        //console.log("dir:"+fdir,"//wr:"+fwra,"//jst:"+fjst,"//a-c:"+falc,"//a-i:"+fali);
        //console.log("//a-s:"+fals,"//gro:"+fgro,"//shr:"+fshr,"//bas:"+fbas);
        var newstyles = '#sandbox{\n';
        newstyles += '    flex-direction: ' + fdir + ';\n';
        newstyles += '    flex-wrap: ' + fwra + ';\n';
        newstyles += '    justify-content: ' + fjst + ';\n';
        newstyles += '    align-content: ' + falc + ';\n';
        newstyles += '    align-items: ' + fali + ';\n';
        newstyles += '}\n';
        newstyles += '.cell{\n';
        newstyles += '    align-self: ' + fals + ';\n';
        newstyles += '    flex-grow: ' + fgro + ';\n';
        newstyles += '    flex-shrink: ' + fshr + ';\n';
        newstyles += '    flex-basis:' + (100/fnum) + '%;\n';
        newstyles += '}\n';
        newstyles += '.cell img{\n';
        newstyles += '    height: ' + ihei + ';\n';
        newstyles += '    width: ' + iwid + ';\n';
        newstyles += '    max-height: ' + imxh + ';\n';
        newstyles += '    max-width: ' + imxw + ';\n';
        newstyles += '}';
        $("#jstyle").html(newstyles);
    }
};
document.addEventListener("DOMContentLoaded", function(event) {
    gojira.init();
});
