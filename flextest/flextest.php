<!DOCTYPE html><html><head><meta charset="utf-8"><title>FlexTest</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ==========CSS=========== -->
    <link href="src/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="flextest.css" rel="stylesheet">
    <!-- ==========JS=========== -->
    <script type="text/javascript" src="src/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="src/bootstrap.min.js"></script>

</head>
<body><style id="jstyle"></style>
<div id="Header"><div id="viheader" unselectable="on" onselectstart="return false;" onmousedown="return false;">Vi FlexTester</div></div>
<?php
include 'inc_panels.php';
?>

<div id="sandbox"></div>

<script type="text/javascript" src="flextest.js"></script>
</body>
</html>


