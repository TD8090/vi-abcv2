<div id="changeling-panel">
    <div class="btn-toolbar panel panel-warning">
        <div id="flex-direction" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-direction</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="row">row</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="row-reverse">row-rev</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="column">column</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="column-reverse">col-rev</label>
        </div>
        <div id="flex-wrap" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-wrap</h4>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="nowrap">nowr</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="wrap">wrap</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="wrap-reverse">wr-rv</label>
        </div>
        <div id="justify-content" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>justify-content</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end </label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-between">space-between</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-around">space-around</label>
        </div>
    </div>
    <div class="btn-toolbar panel panel-warning">
        <div id="align-content" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-content</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="stretch">stretch</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-between">space-between</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-around">space-around</label>
        </div>
        <div id="align-items" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-items</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="stretch">stretch</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="baseline">baseline</label>
        </div>
    </div>
    <div class="btn-toolbar panel panel-success">
        <div id="align-self" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-self</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="auto">auto</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="baseline">baseline</label>
        </div>
        <div id="flex-grow" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-grow</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="0">0</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="1">1</label>
        </div>
        <div id="flex-shrink" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-shrink</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="1">1</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="0">0</label>
        </div>
    </div>
    <div class="btn-toolbar panel panel-success">
        <h3>MISC item-img</h3>
        <div id="height" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>height</h4>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="auto">auto</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="initial">init</label>
        </div>
        <div id="width" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>width</h4>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="auto">auto</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="max-content">maxc</label>
        </div>

        <div id="max-height" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>max-height</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="none">none</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="100%">100%</label>
        </div>
        <div id="max-width" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>max-width</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="none">none</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="100%">100%</label>
        </div>
    </div>

    <div class="btn-toolbar panel panel-success">
        <div id="fbasis" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-basis</h4>
            <input type="number" class="numspin" id="col-spin" value="8" min="1" max="40" step="1" />
            <span id="col-spin-after">cols</span>
        </div>
        <div id="items" class="btn-group btn-group-xs" data-toggle="buttons">
            <h3>-- i   t   e   m   s --</h3>
            <input type="number" class="numspin" id="num-spin"
                   value="10" min="1" max="180" step="1"  />
        </div>
    </div>

    <div class="btn-toolbar panel panel-success">
        <div id="specops" class="btn-group btn-group-sm">
            <h4>SpecOps</h4>
            <button class="btn btn-sm btn-info" id="getdimdiffs">DimDiffs</button>
            <button class="btn btn-sm btn-warning" id="matchfb">Match basis</button>
            <button class="btn btn-sm btn-danger" id="match-heights">Match heights</button>
            <button class="btn btn-sm btn-success" id="organize-cells">Organize Cells</button>
        </div>
    </div>

    <div class="btn-toolbar btn-group-sm panel panel-success text-center">
    </div>
</div>
