<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>PLE</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/PLE_styles.css" rel="stylesheet">
</head><body>
<div class="container"><div id="Header" class="row"><div id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;" unselectable="on" onselectstart="return false;" onmousedown="return false;">Vi PLEncoder</div></div>
    <br/><br/><br/>

    <?php
    /**$thead $tfoot $tblank mk_img() mk_vid() rue()*/
    include 'includes/PLE_inc_parts_kit.php';
    /**$jsonstr  $jsoncount  $filenames*/
    include 'includes/PLE_inc_do_tabletojson.php';
    $v2template = file_get_contents('inc_v2.html');

    $pat_T_SHOW = '/x\.T_SHOW.*?=.*?(\d*);/';
    $pat_T_DELAY = '/x\.T_DELAY.*?=.*?(\d*);/';
    $pat_T_OUT_SCAL = '/x\.T_OUT_SCAL.*?=.*?(\d*);/';
    $pat_T_XFADE = '/x\.T_XFADE.*?=.*?(\d*);/';
    preg_match($pat_T_SHOW, $v2template, $get_T_SHOW);
    preg_match($pat_T_DELAY, $v2template, $get_T_DELAY);
    preg_match($pat_T_OUT_SCAL, $v2template, $get_T_OUT_SCAL);
    preg_match($pat_T_XFADE, $v2template, $get_T_XFADE);
    // setTimeout(function(){seance.fadein(thisP);}, x.T_DELAY);
    // setTimeout(function(){seance.fadeout(thisP);}, x.T_SHOW);   --//doesn't factor in.
    // setTimeout(function(){seance.slide_loop();}, x.T_SHOW + x.T_OUT_SCAL - x.T_XFADE);
    $showDur = ($get_T_SHOW[1]/1000)+($get_T_OUT_SCAL[1]/1000)-($get_T_XFADE[1]/1000);
    echo 'Duration Factor: '.$showDur.' (@4) '.($showDur*4).' (@5) '.($showDur*5).'<br/>';


    $v2split = [];
    $origval = $jsoncount;
    $tempval = $origval;
    //if div by 5, return.   if not, subtract 4, add 1 to 4 counter, and test again
    if($tempval <= 7){$config = [0,0, $tempval];}
    elseif ($tempval == 11) {$config = [1,0,6];}
    else{for ($fours = 0; $fours < 6; $fours++) {
            if ($tempval % 5 == 0) {
                $config = [$tempval / 5, $fours, 0];
                break;
            }
            $tempval = $tempval - 4;
        }
    }
    $endpoints = [];
    $mark = 1;
    //split[0] is fives, iterate through those adding to $v2 array
    for($v=1;$v<=$config[0];$v++){
        array_push($endpoints, $mark, $mark+4);
        $mark = $mark + 5;
    }
    //split[1] is fours, iterate through those adding to $v2 array
    for($v=1;$v<=$config[1];$v++){
        array_push($endpoints, $mark, $mark+3);
        $mark = $mark + 4;
    }
    //split[2] is one unique amount, if exists- just add one iteration to $v2 array
    if($config[2]!=0){
        array_push($endpoints, $mark, $mark+($config[2]-1));
    }
    $v2slide = [];
    $v2proof = [];
    $r=0;
    for($c=0; $c<count($endpoints)/2; $c++){
        print_r($endpoints[$r]."-".$endpoints[$r+1]);
        if($c<(count($endpoints)/2)-1){print_r(" / ");};
        $setDur = ((($endpoints[$r+1] - $endpoints[$r])+1) * $showDur) + (($get_T_DELAY[1]/1000)*2.4); //4 or 5 * show duration
        $v2slide[] = [$setDur,rue(mk_v2($endpoints[$r], $endpoints[$r+1], $v2template, $jsonstr))];
        $v2proof[] = mk_v2($endpoints[$r], $endpoints[$r+1], $v2template, $jsonstr);
        unset($idxA, $idxB);
        $r=$r+2;
    }
    echo '<br/><span style="font:bold 20px \'Courier New\', monospace;">V2 Templates Created: '.count($v2slide).'</span>';
    //add 20 blank templates to $v2[]
    for($v=0;$v<10;$v++){$v2[] = [.001 ,rue($tblank)];}

    $u1 = "http://vi-digital.com/lib/abc/";
    $u2 = "http://vi-digital.com/lib/abc/ABC-Cherry/";
    $u3 = "http://vi-digital.com/lib/vicorporatemenus/";
    $u4 = "http://vi-digital.com/lib/vicorporatemenus/ABC-Corp/";
    /* images i_ [0]TIME, [1]URL, [2]PATH, [3]bgsize*/
    /* videos v_ [0]TIME, [1]URL, [2]PATH, [3]vIURL, [4]vIPATH, [5]VOLUME*/
    $i_bern1   = [9 ,rue(mk_img([$u2,'BernheimPPTforAvery.jpg', 'contain' ]))];
    $i_bern2   = [12,rue(mk_img([$u2,'BernheimPPTforAvery2.jpg', 'cover' ]))];
    $i_burn1   = [9 ,rue(mk_img([$u2,'Burnetts_1.jpg', 'contain' ]))];
    $i_edri1   = [9 ,rue(mk_img([$u2,'Edrington_1.jpg', 'contain' ]))];
    $i_edri2   = [9 ,rue(mk_img([$u2,'Edrington_2.jpg', 'contain' ]))];
    $i_edri3   = [9 ,rue(mk_img([$u2,'Edrington_3.jpg', 'contain' ]))];
    $i_edri4   = [9 ,rue(mk_img([$u2,'Edrington_4.jpg', 'contain' ]))];
    $i_edri5   = [9 ,rue(mk_img([$u2,'Edrington_5.jpg', 'contain' ]))];
    $i_edri6   = [5 ,rue(mk_img([$u2,'Edrington_6.jpg', 'contain' ]))];
    $i_evan1   = [9 ,rue(mk_img([$u2,'EvanW_AppleOrdchard_1.jpg', 'contain' ]))];
    $v_bac150  = [6 ,rue(mk_vid([$u1,'BACARDI150THTVSPOT.ogv', Null, Null, 0.3]))];
    $v_dewars  = [66,rue(mk_vid([$u1,'DewarsnewpackageFlipTheSwitch.ogv', Null, Null, Null]))];
    $v_goose1  = [12,rue(mk_vid([$u2,'Goosevid_A.ogv', Null, Null, Null]))];
    $v_goose2  = [31,rue(mk_vid([$u2,'Goosevid_B.ogv', Null, Null, Null]))];
    $v_goose3  = [18,rue(mk_vid([$u2,'Goosevid_C.ogv', Null, Null, Null]))];
    $v_goose4  = [19,rue(mk_vid([$u2,'Goosevid_D.ogv', Null, Null, Null]))];
    $v_captm1  = [29,rue(mk_vid([$u2,'Rumvid_A.ogv', Null, Null, Null]))];
    $v_captm2  = [20,rue(mk_vid([$u2,'Rumvid_B.ogv', Null, Null, Null]))];
    $v_captm3  = [31,rue(mk_vid([$u2,'Rumvid_C.ogv', Null, Null, Null]))];
    $v_mgbbar  = [34,rue(mk_vid([$u4,'ABCMountGayBlackBarrel.ogv', Null, Null, Null]))];
    $v_mgless  = [19,rue(mk_vid([$u4,'ABCMountGayRum_Lesson_MakeThePerfectRumPunch.ogv', Null, Null, Null]))];
    $v_mgorig  = [19,rue(mk_vid([$u4,'ABCMountGayRum_TheOriginalRum.ogv', Null, Null, Null]))];
    $v_jagspi  = [17,rue(mk_vid([$u4,'JagerSpice.ogv', Null, Null, 0.3]))];
    $vi_bobpe  = [94,rue(mk_vid([$u4,'ABCBobPeters_TitosKombuchaMule.ogv', $u3, 'ABCBobPeters_TitosKombuchaMule-vid-bg.png', Null]))];
    $vi_mmark  = [29,rue(mk_vid([$u2,'MM_mm46_complicated.ogv', $u1, 'MM_mm46_complicated-vid-bg.png', Null]))];

    $i_test1   = [5 ,rue(mk_img([$u2,'EvanW_AppleOrdchard_1.jpg', 'contain' ]))];
    $i_test2   = [5 ,rue(mk_img([$u2,'Burnetts_1.jpg', 'contain' ]))];
    $v_test3   = [.5 ,rue(mk_vid([$u1,'BACARDI150THTVSPOT.ogv', Null, Null, 0.1]))];
    $v_test4   = [.5 ,rue(mk_vid([$u2,'Goosevid_C.ogv', Null, Null, Null]))];

    //    function mk_img($u, $filename, $bgsize)
    //    function mk_vid($v_url, $v_filename, $i_url=Null, $i_filename=Null )

    //build slide array for output
    //[0]= integer for seconds, [1]=encoded string
/*    $slides = [
        [.001 ,rue($tblank)]
        ,$v2slide[0]
        ,$i_test1
        ,$v2slide[1]
        ,$i_test2
        ,$v2slide[2]
        ,$v_test3
        ,$v2slide[3]
        ,$v_test4
        ,$v2slide[4]
        ,$i_test1
        ,$v2slide[5]
    ];*/
    $slides = [
        [.001 ,rue($tblank)]
        ,$v2slide[0]
        ,$v_goose3
        ,$i_edri6
        ,$vi_mmark
        ,$i_edri5
        ,$i_evan1
        ,$v2slide[1]
        ,$v_mgless
        ,$v_bac150
        ,$i_edri2
        ,$v_captm3
        ,$v2slide[2]
        ,$v_goose4
        ,$i_edri1
        ,$v_mgbbar
        ,$v_captm2
        ,$v2slide[3]
        ,$i_edri4
        ,$v_goose2
        ,$i_bern2
        ,$v_goose1
        ,$v2slide[4]
        ,$i_edri3
        ,$v_jagspi
        ,$i_edri1
        ,$v_mgorig
        ,$v2slide[5]
        ,$i_bern1
        ,$vi_bobpe
        ,$v_dewars
        ,$v_captm1
        ,$i_burn1
    ];
    echo '<div class="row"><div class="col-sm-12"><pre id="playlistpre">';
    $numslides = count($slides);
    $pre_output = '';
    $pre_output.= PLcode_above($numslides);
    $pre_output.= "\n\n";
    for($i=0;$i<$numslides;$i++){
        $pre_output.= "PLtime[".$i."]  = ".($slides[$i][0]*1000).";\n";
    }
    $pre_output.= "\n\n";
    for($i=0;$i<$numslides;$i++){
        $pre_output.= "PLcontent[".$i."]  = '".($slides[$i][1])."';\n";
    }
    $pre_output.= "\n\n".PLcode_below()."\n";
    $final_output = $pre_output;
    echo nl2br(htmlspecialchars($pre_output));
    echo '</pre></div></div>';
    echo '<button id="selectallbutton" onclick="selectOutput(\'playlistpre\');"';
    echo ' class="btn btn-info btn-sm btn-block f-nova">Select All</button><br/>';

    file_put_contents("output/out_FINAL.php", $final_output);
    file_put_contents("output/out_Json.log", $jsonstr);
    file_put_contents("output/out_PngList.log", $filelist);
    echo '<a href="output/out_FINAL.php">run: out_FINAL.php</a><br/>';

    $row1 = '<div class="row">';$row2 = '</div>';
    $ta1 = '<textarea cols="50" rows="4">';$ta2 = '</textarea>';
    $sp1 = '<span style="font-size:55px;">';$sp2 = '</span>';


    array_map('unlink', ( glob( "output/out_v2proof*" ) ? glob( "output/out_v2proof*" ) : array() ) );
    for($c=0; $c<count($v2proof); $c++){
        file_put_contents("output/out_v2proof".($c+1).".php", $v2proof[$c]);
        echo $row1.$sp1.'v2proof'.($c+1).$sp2.$ta1.$v2proof[$c].$ta2.$row2;
        echo '<a href="output/out_v2proof'.($c+1).'.php">run: v2proof'.($c+1).'.php </a><br/>';
    }

    ?>
</div>


<!--/////end:(BODY)/////-->

<!-- ==========JS=========== -->
<script src="src/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="src/bootstrap.min.js"></script>
<script>

    function selectOutput( containerid ) {

        var node = document.getElementById( containerid );

        if ( document.selection ) {
            var range = document.body.createTextRange();
            range.moveToElementText( node  );
            range.select();
        } else if ( window.getSelection ) {
            range = document.createRange();
            range.selectNodeContents( node );
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( range );
        }
    }

</script>
</body>
</html>