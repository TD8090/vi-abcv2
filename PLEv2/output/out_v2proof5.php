<html><head><meta charset="utf-8"><title>v2</title>
    <style>html, body {background-color:#000000;border:none;color:#ffffff;margin:0; height:100%; width:100%;padding:0;}</style>
    <script type="text/javascript">
        seance = {
            'globals':function(){var x=this;
                x.ID_jstylefb = document.getElementById('jstyle-fb');
                x.ID_jstylepr = document.getElementById('jstyle-pr');
                x.ID_gridcont = document.getElementById("grid-container");
                x.ID_prescont = document.getElementById("pres-container");
                document.getElementById("title-area").textContent = "October Specials";
                x.PrSTART  = 21;//R__PRSTART
                x.PrEND    = 24;//R__PREND
                x.jd = [{"code":"22-420","name":"George Dickel Rye","size":".75L","prices":{"savings":"3.00","discount":"22.95","retail":"25.95"},"filename":"22420_George_Dickel_Rye.png"},{"code":"23-562","name":"Elijah Craig (6)","size":"1.75L","prices":{"savings":"5.00","discount":"46.95","retail":"51.95"},"filename":"23562_Elijah_Craig_6.png"},{"code":"24-087","name":"Jim Beam Red Stag","size":"1.75L","prices":{"savings":"3.00","discount":"26.95","retail":"29.95"},"filename":"24087_Jim_Beam_Red_Stag.png"},{"code":"25-598","name":"Wild Turkey 81","size":".75L","prices":{"savings":"3.00","discount":"18.95","retail":"21.95"},"filename":"25598_Wild_Turkey_81.png"},{"code":"31-274","name":"Jim Beam Honey","size":"1.75L","prices":{"savings":"3.00","discount":"26.95","retail":"29.95"},"filename":"31274_Jim_Beam_Honey.png"},{"code":"34-357","name":"Dalwhinnie Classic 15Y S.M.","size":".75L","prices":{"savings":"6.00","discount":"48.95","retail":"54.95"},"filename":"34357_Dalwhinnie_Classic_15Y_SM.png"},{"code":"35-487","name":"Grand MacNish","size":"1.75L","prices":{"savings":"3.00","discount":"18.95","retail":"21.95"},"filename":"35487_Grand_MacNish.png"},{"code":"38-547","name":"Rich & Rare Reserve Canadian","size":"1.75L","prices":{"savings":"5.00","discount":"19.95","retail":"24.95"},"filename":"38547_Rich_&_Rare_Reserve_Canadian.png"},{"code":"40-122","name":"Jameson","size":".75L","prices":{"savings":"3.00","discount":"25.95","retail":"28.95"},"filename":"40122_Jameson.png"},{"code":"42-950","name":"Bombay","size":".75L","prices":{"savings":"3.00","discount":"16.95","retail":"19.95"},"filename":"42950_Bombay.png"},{"code":"43-939","name":"UV Vodka","size":"1.75L","prices":{"savings":"3.00","discount":"15.95","retail":"18.95"},"filename":"43939_UV_Vodka.png"},{"code":"44-238","name":"Tito's Handmade Vodka","size":".75L","prices":{"savings":"3.00","discount":"18.95","retail":"21.95"},"filename":"44238_Titos_Handmade_Vodka.png"},{"code":"44-315","name":"Deep Eddy Ruby Red Grapefruit","size":".75L","prices":{"savings":"3.00","discount":"15.95","retail":"18.95"},"filename":"44315_Deep_Eddy_Ruby_Red_Grapefruit.png"},{"code":"46-538","name":"Finlandia","size":"1.75L","prices":{"savings":"3.00","discount":"24.95","retail":"27.95"},"filename":"46538_Finlandia.png"},{"code":"46-581","name":"Stolichnaya 100","size":".75L","prices":{"savings":"4.00","discount":"21.95","retail":"25.95"},"filename":"46581_Stolichnaya_100.png"},{"code":"46-752","name":"Ciroc Pineapple","size":".75L","prices":{"savings":"4.00","discount":"29.95","retail":"33.95"},"filename":"46752_Ciroc_Pineapple.png"},{"code":"47-107","name":"Zaya Rum(6)","size":".75L","prices":{"savings":"5.00","discount":"29.95","retail":"34.95"},"filename":"47107_Zaya_Rum6.png"},{"code":"47-118","name":"Calico Jack Coconut Rum","size":"1.75L","prices":{"savings":"3.00","discount":"16.95","retail":"19.95"},"filename":"47118_Calico_Jack_Coconut_Rum.png"},{"code":"49-241","name":"Capt. Morgan Spiced 100","size":".75L","prices":{"savings":"3.00","discount":"17.95","retail":"20.95"},"filename":"49241_Capt_Morgan_Spiced_100.png"},{"code":"51-056","name":"Ansac VS","size":".75L","prices":{"savings":"3.00","discount":"21.95","retail":"24.95"},"filename":"51056_Ansac_VS.png"},{"code":"61-943","name":"Evan Williams Honey","size":"1.75L","prices":{"savings":"3.00","discount":"22.95","retail":"25.95"},"filename":"61943_Evan_Williams_Honey.png"},{"code":"61-956","name":"Southern Comfort 100","size":"1.75L","prices":{"savings":"5.00","discount":"34.95","retail":"39.95"},"filename":"61956_Southern_Comfort_100.png"},{"code":"63-409","name":"Chambord Royale","size":".75L","prices":{"savings":"4.00","discount":"29.95","retail":"33.95"},"filename":"63409_Chambord_Royale.png"},{"code":"63-972","name":"Harlequin Orange Liq. w\/ Cognac(6)","size":".75L","prices":{"savings":"3.00","discount":"16.95","retail":"19.95"},"filename":"63972_Harlequin_Orange_Liq_w_Cognac6.png"},{"code":"65-072","name":"Sauza Blue Silver Tequila","size":".75L","prices":{"savings":"3.00","discount":"16.95","retail":"19.95"},"filename":"65072_Sauza_Blue_Silver_Tequila.png"},{"code":"65-128","name":"Two Fingers Silver","size":"1.75L","prices":{"savings":"3.00","discount":"22.95","retail":"25.95"},"filename":"65128_Two_Fingers_Silver.png"},{"code":"65-219","name":"Margaritaville Gold","size":"1.75L","prices":{"savings":"3.00","discount":"22.95","retail":"25.95"},"filename":"65219_Margaritaville_Gold.png"},{"code":"65-418","name":"Lunazul Reposado","size":"1.75L","prices":{"savings":"5.00","discount":"29.95","retail":"34.95"},"filename":"65418_Lunazul_Reposado.png"}];//R__JDATA
                x.jlen = x.jd.length;
                x.T_DELAY = 900;
                x.T_SHOW  = 8400;
                x.T_IN_SCAL = 900;
                x.T_IN_OPAC = 900;
                x.T_OUT_SCAL = 2000;
                x.T_OUT_OPAC = 1800;
                x.T_XFADE = 2200;
                x.URL_SLUG    = "http://vi-digital.com/lib/abc/ABC-Cherry/";
                x.LOC_SLUG    = "";
                x.NOIMG_LOC   = "!no_image_ABC.svg";
                x.NOIMG_RMT   = "http://vi-digital.com/lib/abc/ABC-Cherry/!no_image_ABC.svg";
                x.gridboxes= [];
                x.PrIdx = 0;
                x.PrIdxLast = x.PrEND - x.PrSTART;//4 or 5
                x.imgloadcount = 0;
            }

            ,'init':function(){var x=this;
                x.globals();
                x.buildGrid();
                x.buildPres();
            }
            ,'imgLoadCounter':function(){
                this.imgloadcount++;
                if(this.imgloadcount == this.jlen ) {this.init2();}
            }
            ,'init2':function(){var x=this;
                x.cellz = document.getElementsByClassName('cell');
                x.organizeCellsByWidth();
                x.setJstylefb();
                x.setJstylepr();
                x.p_bases = document.getElementsByClassName('Pbase');
                x.slide_loop();
            }
            ,'slide_loop':function(){
                var x=this, thisP = x.p_bases[x.PrIdx];
                if (x.PrIdx <= x.PrIdxLast) {
                    setTimeout(function(){seance.fadein(thisP);}, x.T_DELAY);
                    setTimeout(function(){seance.fadeout(thisP);}, x.T_SHOW);
                    if (x.PrIdx < x.PrIdxLast){
                        x.PrIdx++;
                        setTimeout(function(){seance.slide_loop();}, x.T_SHOW + x.T_OUT_SCAL - x.T_XFADE);
                    }else{/*loop: x.PrIdx=0;*/}
                }
            }
            ,'fadein':function(Ef){Ef.classList.remove('moveout');Ef.classList.add('movein');}
            ,'fadeout':function(Ef){Ef.classList.remove('movein');Ef.classList.add('moveout');}
            ,'buildPres':function(){var x = this, preshtml = '';
                for (var i = (x.PrSTART - 1); i <= (x.PrEND - 1); i++) {
                    preshtml += '<div id="Pr'+(i+1)+'" class="Pwrap"><table class="Pbase moveout">'
                            + '<td class="Pleft"><img src="' + x.LOC_SLUG + x.jd[i].filename + '" onerror="seance.imgError1(this);" />'
                            + '</td><td class="Pright"><div class="Ptitle">' + x.jd[i].name + '</div><hr>'
                            + '<div class="Pregprice">Regular Price: $' + x.jd[i].prices.retail + '</div>'
                            + '<div class="Psavings">Savings: $' + x.jd[i].prices.savings + '</div>'
                            + '<div class="Psaleprice">Sale Price: $' + x.jd[i].prices.discount + '</div></td></table></div>';
                }x.ID_prescont.innerHTML = preshtml;
            }
            ,'setJstylepr' : function(){
                this.ID_jstylepr.innerHTML = '.movein {\r\n  opacity: 1;\r\n'
                        +'  -webkit-transform: scale(1);\r\n  transform: scale(1);\r\n'
                        +'  -webkit-transition: -webkit-transform '+this.T_IN_SCAL+'ms, opacity '+this.T_IN_OPAC+'ms;\r\n'
                        +'  transition:  transform '+this.T_IN_SCAL+'ms, opacity '+this.T_IN_OPAC+'ms;;\r\n}\r\n'
                        +'.moveout {\r\n  opacity: 0;\r\n'
                        +'  -webkit-transform: scale(0);\r\n  transform: scale(0);\r\n'
                        +'  -webkit-transition: -webkit-transform '+this.T_OUT_SCAL+'ms, opacity '+this.T_OUT_OPAC+'ms;\r\n'
                        +'  transition:  transform '+this.T_OUT_SCAL+'ms, opacity '+this.T_OUT_OPAC+'ms;\r\n}\r\n';
            }
            ,'organizeCellsByWidth':function() {var A= [];
                A.slice.call(this.cellz).sort(function(a,b){
                    return parseInt(a.firstChild.naturalWidth, 10) - parseInt(b.firstChild.naturalWidth, 10);
                }).forEach(function(next){
                    seance.ID_gridcont.appendChild(next);
                });
            }
            ,'setJstylefb':function(){
                var x = this,n_items = x.jlen,final_lowoffset_colnum = 0,final_lowoffset_value = 0;
                for(var colnum = 1; colnum<=20; colnum++) {
                    var remrow = (n_items % colnum == 0)? colnum : n_items % colnum;
                    if (n_items >= colnum && remrow >= colnum/2) {
                        x.ID_jstylefb.innerHTML = '.cell {flex-basis:'+100/colnum+'%;}';
                        var colset_lowest_oheight = 1000, A= [];
                        A.slice.call(this.cellz).forEach(function(elmt){
                            if(colset_lowest_oheight > elmt.firstChild.offsetHeight){
                                colset_lowest_oheight = elmt.firstChild.offsetHeight;
                            }}
                        );
                        if(colset_lowest_oheight >= final_lowoffset_value){
                            final_lowoffset_colnum = colnum;
                            final_lowoffset_value = colset_lowest_oheight;}
                    }
                }
                x.ID_jstylefb.innerHTML = '.cell {flex-basis:'+100/final_lowoffset_colnum+'%;}\r\n'
                        +'.item-img { height:'+final_lowoffset_value+'px; }';
            }

            ,'imgError1': function(image){/*console.log("==imgError1 : TRYING REMOTE SRC ==", image);*/
                var fname = image.src.match(/.*\/(.*\.png|gif|svg)/);
                image.src = null;image.onerror = null;image.removeAttribute("onerror");
                image.setAttribute("onerror", "seance.imgError2(this);");
                image.src = this.URL_SLUG+fname[1];return true;
            }
            ,'imgError2': function(image) {/*console.log("==imgError2 : TRYING SVG LOCAL ==");*/
                image.src= null;image.onerror = null;image.removeAttribute("onerror");
                image.setAttribute("onerror", "seance.imgError3(this);");
                image.src = this.NOIMG_LOC;return true;
            }
            ,'imgError3': function(image) {/*console.log("==imgError3 : TRYING SVG REMOTE ==");*/
                image.src= null;image.onerror = null;image.removeAttribute("onerror");
                image.src = this.NOIMG_RMT;return true;
            }

            ,'buildGrid':function() {var x = this;
                for (var i = 0; i < x.jlen; i++) {
                    var div = document.createElement('div');
                    var img = document.createElement('img');
                    div.setAttribute('class', 'cell');
                    img.src = x.LOC_SLUG + x.jd[i].filename ;
                    img.onerror= function(){seance.imgError1(this);};
                    img.onload = function(){seance.imgLoadCounter();};
                    img.setAttribute('class', 'item-img');
                    div.appendChild(img);
                    x.ID_gridcont.appendChild(div);
                }
            }
        };
        function startSlideshow(){
            seance.init();
        }

    </script>
    <style type="text/css">body { background:#FFFFFF;box-sizing: border-box; /*overflow:hidden;*/}
    body{ width:1280px; height:768px;}
    #grid-container {background:#aaaaaa;position: relative;height: 626px;padding:10px;
        display:flex;flex-wrap: wrap;justify-content: space-around;
    }
    .cell {position:relative;width:auto;height:auto;
        align-items: center;flex-grow:1;flex-basis:5%;
    }
    .item-img {position: absolute;top:0;bottom:0;left:0;right:0;
        margin: auto auto;
        max-width:100%;
        max-height:100%;
    }
    #title-area{
        width: 100%;
        height: 120px;
        font-size: 95px;
        line-height:145px;
        background: #ff0000;
        border-bottom: 2px solid #CC0000;
        color: #FFFFFF;text-transform: uppercase;
        font-family:  'Helvetica', sans-serif;text-align: center;
        font-weight: 600;
        text-shadow:4px  4px 0 #c40000,-1px -1px 0 #c40000,1px -1px 0 #c40000,-1px  1px 0 #c40000,1px  1px 0 #c40000;}
    .Pwrap{
        position: absolute;
        top: 110px;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        width:1280px; height:658px;
    }
    .Pbase {opacity:0;max-width:85%;
        border-collapse:separate;
        border-spacing:30px;
        background: rgba(246, 246, 246, 0.95);
        border: 3px solid #aaaaaa; border-radius: 16px;-webkit-box-shadow: -6px 6px 15px 0 rgba(0,0,0,0.4);box-shadow:         -6px 6px 15px 0 rgba(0,0,0,0.4);
        -webkit-transform:scale(0,0); transform:scale(0,0);}
    .Pright{max-width:700px;vertical-align: top;}
    .Pleft {max-width:500px;width:auto;}
    .Pleft img{max-width:100%;}
    .Ptitle,.Pregprice,.Psaleprice,.Psavings{color: #666666;font-family:'Helvetica',sans-serif;font-weight:600;}
    .Ptitle{font-size:54px;line-height:64px;color:#000000;}
    .Pregprice {font-size:43px;line-height:79px;}
    .Psavings  {font-size:43px;line-height:79px;}
    .Psaleprice{font-size:48px;line-height:82px;color:red;font-weight:bold;}
    </style>
</head>
<body>
<style id="jstyle-fb"></style>
<style id="jstyle-pr"></style>
<div id="title-area"></div>
<div id="grid-container"></div>
<div id="pres-container"></div>
<!-- <script>startSlideshow();</script> -->
<script>
    window.contentType = 'slideshow';
</script>
</body>
</html>