<?php
//body,html,table,td,div,video,img,p,*{cursor:none;}
$thead = <<<HTML
<html><head><style>body {background-color:#000000;border:none;color:#ffffff;overflow:hidden;}</style></head><body>
HTML;
$tfoot = <<<HTML
</body></html>
HTML;
$tblank = <<<HTML
<!doctype html><html><head><meta charset="utf-8">
<style>
body, html {
    background-color: #000000;
    border: none;
    color: #ffffff;
    overflow: hidden;
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
}
</style>
</head><body></body></html>
HTML;

/* rue()
=============================================================*/
function rue($a){return rawurlencode($a);}

/* mk_img() - [0]IMGURL, [1]IMGPATH, [2]bgsize(contain/cover)
=============================================================*/
function mk_img($arg){
return <<<HTML
<html><head><style>/*body,html,table,td,div,video,img,p,*{cursor:none;}*/
body {border:none;overflow:hidden;}
body, html {background-color:#000000; color:#000;
background-image:url('$arg[1]'), url('$arg[0]$arg[1]');
background-repeat:no-repeat;background-position:center;height:100%; width:100%;padding:0; margin:0;
-webkit-background-size: $arg[2];-moz-background-size: $arg[2];
-o-background-size: $arg[2];background-size: $arg[2];}
p{margin:0;padding:0;}
</style></head><body>
</body></html>
HTML;
}

/* mk_vid() - [0]vidURL, [1]vidPATH, [2]vIMGURL, [3]vIMGPATH, [4]VOLUME(null, 0.0 - 1.0)
========================================================================================*/
function mk_vid( $arg ){
    $output = '<html><head><style>';//    $output .= 'body,html,table,td,div,video,img,p,*{cursor:none;}';
    $output .= 'body {overflow:hidden; background-color:#000000; border:none; color:#000; margin:0; padding:0; height:100%';
    if($arg[2]!==Null && $arg[3]!==Null):
    $output .= 'background-image:url('.$arg[3].'), url('.$arg[2].$arg[3].');';
	$output .= 'background-repeat:no-repeat; padding:0; height:100%;';
    endif;
    $output .= <<<HTML
    }</style></head><body>
<video id="vid" style="position:absolute; top:0; left:0; width:100%; height:100%;">
<source src="$arg[1]">
<source src="$arg[0]$arg[1]">
</video>
HTML;
    if($arg[4]!==Null) {
        $output .= '<script type="text/javascript">';
        $output .= 'var video = document.getElementById("vid");';
        $output .= 'video.onplay = function(){video.volume = ' . $arg[4] . ';}';
        $output .= '</script>';
    }

    $output .= '</body></html>';
    return $output;
}

/* mk_v2() - [0]start, [1]end, [2]original inc_v2.html, [3]json string
========================================================================================*/
function mk_v2($st, $en, $orig, $jd){
    $pat_st = '/(x.PrSTART.*?=\s+?)(.*)(;\s*?\/\/R__PRSTART)/';
    $rep_st = '${1}'.$st.'${3}';
    $x = preg_replace($pat_st, $rep_st, $orig);
    $pat_en = '/(x.PrEND.*?=\s+?)(.*)(;\s*?\/\/R__PREND)/';
    $rep_en = '${1}'.$en.'${3}';
    $x = preg_replace($pat_en, $rep_en, $x);
    $pat_jd = '/(x.jd.*?=\s+?)(.*)(;\s*?\/\/R__JDATA)/';
    $rep_jd = '${1}'.$jd.'${3}';
    $x = preg_replace($pat_jd, $rep_jd, $x);
    $pat_sss = '/(<\w*?>startSlideshow\(\);<\/script>)/';
    $rep_sss = '<!-- $1 -->';
    $x = preg_replace($pat_sss, $rep_sss, $x);
    return $x;
}


/* PLcode_above (numslides)
========================*/
function PLcode_above ($numslides){
    $output = <<<HTML
<html><head><title>out_FINAL</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style>/*html, body { cursor:none; }*/
    body { background-color:#000000;border:none;color:#ffffff;overflow:hidden; margin:0; padding:0; width:100%; height:100%; }
    #pl-cover {
        background-color: #000000;color: #000000;
        position:absolute; top:0; left:0;
        font-family: sans-serif;
        text-align: center;
        width:1280px; height:768px;
        z-index:10000;
    }
</style></head>
<body class="vipsystem">
<div id="pl-cover"><br />
<br /><br /><br /><br />
Loading playlist... Please wait.</div>
<script type="text/javascript">

        // ADJUSTABLE STUFF ================================================================
        var preload       = 100;      /* Let each frame load X/ms onCreate               */
        var fade_speed    = 60;       /* FADE SPEED - 10=fast, 100=slow                  */
        var slides        = $numslides;        /* Number of frames                                */
        // END ADJUSTABLE STUFF ============================================================

        var plnum = 0;
        var plast = 0;
        var PLtime = new Array(slides);
        var PLcontent = new Array(slides);
HTML;
    return $output;}

/* PLcode_below (numslides)
========================*/
function PLcode_below (){
    $output = <<<JS
        function wipeFrame (num) {
            var iframe = (document.getElementById('Frame'+num).contentWindow || document.getElementById('Frame'+num).contentDocument);
            if (iframe.window.contentType == 'slideshow') {
                iframe.clearTimeout(iframe.window.switchSlide);
                iframe.document.open();
                iframe.document.write('slideshow');
                iframe.document.close();
            }
        }
        function Opac (opac) {
            if (opac <= 10) {
                document.getElementById('Frame'+plnum).style.opacity = (opac / 10);
                if (opac == 10) {
                    document.getElementById('Frame'+plast).style.opacity = 0;
                }
            }
        }
        function Opac2 (opac) {
            if (opac >= 0) {
                document.getElementById('Frame'+plast).style.opacity = (opac / 10);
                if (opac == 0) {
                    document.getElementById('Frame'+plast).style.opacity = 0;
                }
            }
        }
        function FrameIn () {
            iframe = (document.getElementById('Frame'+plnum).contentWindow || document.getElementById('Frame'+plnum).contentDocument);
            var opac = document.getElementById('Frame'+plnum).style.opacity;
            if (opac < 1) {
                for (var i = 1; i<=10; i++) {
                    setTimeout('Opac('+i+')', fade_speed*i);
                }
            }
            var video =  document.getElementById('Frame'+plnum).contentWindow.document.getElementsByTagName('video');
            if ( video.length > 0 ) {
                video[0].play();
            } else {
                var iframe = document.getElementById('Frame'+plnum);
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.write(rawurldecode(PLcontent[plnum]));
                iframe.contentWindow.document.close();
                if (iframe.contentWindow.window.contentType == 'slideshow') {
                   iframe.contentWindow.startSlideshow();
                }
            }
        }
        function FrameOut () {
            var opac = document.getElementById('Frame'+plast).style.opacity;
            if (opac > 0) {
                for (var i = 10, j=0; i>=0; i--) {
                    j++;
                    setTimeout('Opac2('+i+')', fade_speed*j);
                }
            }
        }
        function rawurldecode(str) {
            return decodeURIComponent((str + '')
                    .replace(/%(?![\da-f]{2})/gi, function() {
                        return '%25';
                    }));
        }
        function nextPlaylistSlide() {
            for(var i = 0; i <= slides-1; i++){
                if (i != plast) {
                    document.getElementById('Frame'+i).style.opacity = 0;
                } else {
                    document.getElementById('Frame'+i).opacity = 1;
                }
            }
            if (plast+1 == slides) {
                document.getElementById('Frame'+plnum).style.opacity = 1;
                FrameOut();
            } else {
                FrameIn();
            }
            setTimeout(function() {
                plast = plnum;
                plnum++;
                if (plnum > slides-1) plnum=0;
                var iframe = (document.getElementById('Frame'+plnum).contentWindow || document.getElementById('Frame'+plnum).contentDocument);
                if (iframe.document.body.innerHTML == 'slideshow') {
                    iframe.document.open();
                    iframe.document.write(rawurldecode(PLcontent[plnum]));
                    iframe.document.close();
                }
                setTimeout(function () {
                    wipeFrame(plast);
                }, fade_speed*10);
                nextPlaylistSlide();
            }, PLtime[plnum]);
        }
        function cycleFrames () {
            setTimeout('cycleFrames()', 1000);
            for (var i = 0; i <= slides-1; i++) {
                if (i != plnum) {
                    var video, q;
                    if (document.getElementById('Frame'+i).contentWindow) {
                        video = document.getElementById('Frame'+i).contentWindow.document.getElementsByTagName('video');
                    }
                    var vid_len = video.length;
                    if (vid_len > 0) {
                        for(q=0;q<vid_len;q++) {
                            if(video[q].currentTime > 0) {
                                if (video[q].readyState == 4) {
                                    video[q].currentTime = 0; // sets the video to play from beginning
                                    video[q].pause();
                                }
                            }
                        }
                    } /*else {
                        var iframe = document.getElementById('Frame'+i);
                         if (iframe.contentWindow.window.contentType == 'slideshow') {
                         iframe.contentWindow.clearTimeout(iframe.contentWindow.window.switchSlide);
                         }
                    }*/
                }
            }
        }
        function makeFrame (i) {
            var iframe = document.createElement('iframe');
            document.body.appendChild(iframe);
            iframe.transparency   = 'true';
            iframe.style.opacity  = 1;
            iframe.scrolling      = 'no';
            iframe.style.position = 'absolute';
            iframe.style.top      = '0px';
            iframe.style.left     = '0px';
            iframe.style.width    = '1280px';
            iframe.style.height   = '768px';
            iframe.style.border   = '0px';
            iframe.id             = 'Frame'+i;
            iframe.zIndex         = i+1;
            iframe.contentWindow.document.open();
            iframe.contentWindow.document.write(rawurldecode(PLcontent[i]));
            iframe.contentWindow.document.close();
            var video =  iframe.contentWindow.document.getElementsByTagName('video');
            if ( video.length > 0 ) {
                video[0].pause();
            }
            if (iframe.contentWindow.window.contentType == 'slideshow') {
                iframe.contentWindow.clearTimeout(iframe.contentWindow.window.switchSlide);
                if (i != 0) {
                    iframe.contentWindow.document.open();
                    iframe.contentWindow.document.write('slideshow');
                    iframe.contentWindow.document.close();
                }
            }
            if (i == slides-1) {
                document.getElementById('pl-cover').style.display = 'none';
            }
        }
        function setupStage() {
            for (var i = 0; i <= slides-1; i++) {
                setTimeout('makeFrame('+i+')', preload*i);
            }
            setTimeout('nextPlaylistSlide()', slides*(preload+1));
            setTimeout('cycleFrames()', slides*(preload+1));
        }
        setupStage();
JS;
    $output .= '</script></body></html>';
    return $output;
}
