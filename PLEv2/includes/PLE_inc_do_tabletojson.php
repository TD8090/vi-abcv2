<?php
$input=file_get_contents("includes/input_specials_table.txt");
//build $lines array, now we have an array of lines
$lines = explode("\r\n", $input);
//go through lines and turn lines into fields
foreach ($lines as $l) {
    $line[] = explode("	", $l);
}unset($lines);
//$e is each line's index, $f is the array which holds values within that line
foreach ($line as $e => $f) {
    foreach ($f as $field) {
        if (preg_match("/\d\d\-\d\d\d/i", $field)) $specials[$e]['code'] = trim($field);
        elseif (preg_match("/.*\.75L/i", $field)) $specials[$e]['size'] = trim($field);
        elseif (preg_match("/\w{3,32}/i", $field)) $specials[$e]['name'] = trim($field);
        elseif (preg_match("/^\\$\d/i", $field)) $specials[$e]['prices'][] = trim(str_replace("$", "", $field));
    }
}unset($line);
$filename = "";
$filelist = "\r\n";
//sort prices ascending in [prices] array
foreach ($specials as &$special) {
    $filename = str_replace("-", "", $special['code']);
    $filename .= '_';
    $filename .= preg_replace('/\s/', '_', preg_replace("([^\w\s\d\-_~,&])", "", $special['name']));
    $filename .= ".png";
    $special['filename'] = $filename;
    $filelist .= $filename."\r\n";
    unset($filename);
    sort($special['prices']);
    $special['prices']['savings'] = $special['prices']['0'];    unset($special['prices']['0']);
    $special['prices']['discount'] = $special['prices']['1'];   unset($special['prices']['1']);
    $special['prices']['retail'] = $special['prices']['2'];     unset($special['prices']['2']);
}unset($special);
//print_r($specials);
$jsoncount = count($specials);
$jsonstr = json_encode($specials);
//    print_r($specials);
//    print_r($filenames);
